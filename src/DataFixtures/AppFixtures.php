<?php

namespace App\DataFixtures;

use Faker\Factory;
use Faker\Generator;
use App\Entity\User;
use Doctrine\Bundle\FixturesBundle\Fixture;
use Doctrine\Persistence\ObjectManager;

class AppFixtures extends Fixture
{
     /** 
    *var Generator
    */
    private Generator $faker;
    public function load(ObjectManager $manager): void
    {
        $this->faker = Factory::create('fr_FR');
        // $product = new Product();
        // $manager->persist($product);
        for($i=0; $i<10; $i++){
            $user = new User;
            $user
            ->setEmail($this->faker->unique()->email())
            ->setPseudo($this->faker->unique()->userName())
            ->setPassword(password_hash($this->faker->password(),PASSWORD_DEFAULT));

            $manager->persist($user);
        }
        
        $manager->flush();
    }

}
