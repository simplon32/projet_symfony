<?php

namespace App\Controller;

use App\Entity\User;
use App\Repository\ScoresRepository;
use App\Repository\UserRepository;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\BrowserKit\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;



class UsersController extends AbstractController
{
     #[Route('/users', name: 'app_users')]
    // public function index(ScoresRepository $scoresRepo): Response
    // {
    //     return $this->render('users/index.html.twig', [
    //         'scores' => $scoresRepo ->findBy([],['score' => 'desc']),
    //     ]);
    // }
  
    public function index(ScoresRepository $scoresRepo): Response
    {
        return $this->render('users/index.html.twig');
     }

    #[Route('/new', name: 'app_user_new', methods: ['GET', 'POST'])]
    public function new(Request $request, UserRepository $UserRepository): Response
    {
        $utilisateur = new User();
        $form = $this->createForm(UserType::class, $utilisateur);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $UserRepository->save($utilisateur, true);

            return $this->redirectToRoute('app_user_index', [], Response::HTTP_SEE_OTHER);
        }

        return $this->renderForm('user/new.html.twig', [
            'user' => $utilisateur,
            'form' => $form,
        ]);
    }

}
