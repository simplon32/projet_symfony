<?php

namespace App\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use App\Entity\Scores;
use App\Repository\ScoresRepository;
use App\Repository\UserRepository;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Doctrine\Persistence\ManagerRegistry;

class ScoresController extends AbstractController
{
    #[Route('/scores', name: 'app_scores')]
    public function index(): Response
    {
        return $this->render('scores/index.html.twig', [
            'controller_name' => 'ScoresController',
        ]);
    }

    public function __construct(private ManagerRegistry $doctrine) {}
    /**
     * @Route("/scores_get", name="scores_get", methods={"GET"})
     */
    public function ScoresUserGet(){
        $user = $this ->getUser();
        $idUser = $user -> getId();
        //on récupère dans la BDD les scores par ordre décroissant
    $scores = $this->doctrine->getRepository(Scores::class)->findBy(
        ['user' => $idUser],
        ['Date' => 'DESC']);
    $jsonData = array();
    $idx = 0;
    foreach ($scores as $score){
        //on rentre dans le tableau les scores
        $temp = array(
            'score' => $score->getScore()
        );
        $jsonData[$idx++] = $temp;
    }
    return new JsonResponse($jsonData);
    }
    /**
     * @Route("/scores_post", name="scores_post", methods={"POST"})
     */
    public function ScoresPost(Request $request){
        $nouveauScore = $request->get('valeurScore');
        $scores = $this->getDoctrine()->getRepository(Scores::class)->save($nouveauScore);
    
    }
    public function createScore(Request $request, ScoresRepository $scoresRepo, UserRepository $usersRepo):Response
    {
        //j'instancie la classe score et lui assigne ses valeurs d'attribut
        $scoreObjet = new Scores;
        $scoreObjet -> setScore('{valeurScore}');
        $scoreObjet -> setDate(time());
        $user = $this ->getUser();
        $idUser= $user->getId();
        $scoreObjet ->setUser($idUser);

        $scoresRepo -> save($scoreObjet);
        return ${valeurScore};
    }
}
